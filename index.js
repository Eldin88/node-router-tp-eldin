// Import des libs
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const posts = require("./posts.json");
// Instanciation du serveur Web
const app = express();
// Configuration de Express
const PORT = 4000;
app.use(cors());
app.use(morgan("combined"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// Déclaration des routes
app.get("/", (request, response) => {
response.send("Wesh");
});
app.get("/posts", (request, response) => {
response.json(posts);
});
// Lancement du serveur
app.listen(PORT, () => console.log(`Je suis lancé sur le port ${PORT}`));
[
    {
    "id": "B4",
    "title": "Vive les cactus",
    "contenu": "Je suis fan de cactus parce que les cactus c'est bien.",
    "path": "/cactus"
    },
    {
    "id": "35b",
    "title": "Vive les arbres",
    "contenu": "Je suis fan des arbres parce que les arbres c'est bien.",
    "path": "/arbres"
    },
    {
    "id": "M42",
    "title": "Vive les mongolfières",
    "contenu": "Je suis fan de mongolfières parce que les mongolfières c'est bien.",
    "path": "/mglfrs"
    }
    ]